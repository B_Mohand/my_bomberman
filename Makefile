CC=gcc
CFLAGS=-Wall -Wextra -Werror `sdl-config --cflags --libs` -lSDL_image 
NAME=bomberman

$(NAME): main.c
	$(CC) $(CFLAGS) -o $(NAME) main.c
all : $(NAME)
clean:
	rm -rf *.o
fclean : clean
		rm -f $(NAME)
re : fclean